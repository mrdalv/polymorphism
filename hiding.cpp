/*
    3. Изменение спецификатора доступа метода, сокрытие метода/переменной
   родительского класса
*/

#include <iostream>

class Animal {
   public:
    // Здесь он публичен, а в дочернем классе будет скрыт
    int m_age = 23;

    Animal(std::string name) : m_name(name) {}

    std::string getColor() { return m_color; }

    ~Animal() { std::cout << "Animal ..." << std::endl; }

   protected:
    void printName() const { std::cout << m_name << std::endl; }

   private:
    std::string m_name;
    std::string m_color;
};

class Dog : public Animal {
   public:
    Dog(std::string name, int age) : Animal(name) { m_age = age; }

    // Удаление метода родительского, без возможности использования его в
    // дальнейшем
    std::string getColor() = delete;

    // Возможность использования родительского метода
    using Animal::printName;

    ~Dog() { std::cout << "Dog ..." << std::endl; }

   private:
    // Переопределение в приват
    using Animal::m_age;
};

int main(int argc, char const *argv[]) {
    Dog dog("Bobik", 10);

    dog.printName();

    // Не сработает, т.к. произошло сокрытие переменной внутри дочернего класса
    // std::cout <<  dog.m_age << std::endl;

    // Не сработает, т.к. произошло удаление внутри дочернего класса
    // std::cout << dog.getColor() << std::endl;

    return 0;
}
